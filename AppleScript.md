## 支持的命令
用户可以通过AppleScript进行调用PD Runner, 以满足自动化操作需求. PD Runner支持下列命令:  
`start vm "name" [with password "pass"]`, `start macvm`, `list all`, `start all`, `stop all`, `hide icon`, `show icon`

## 使用示例
👉**使用AppleScript接口之前请先运行PD Runner, 或执行:**  
```AppleScript
tell application "PD Runner" to activate
```  
---
启动一个名为"Windows 11"的虚拟机:  
```AppleScript
tell application "PD Runner" to start vm "Windows 11"
```  
如果虚拟机处于加密状态: 
```AppleScript
tell application "PD Runner" to start vm "Windows 11" with password "foobar"
```  
启动基于Apple Silicon的macOS虚拟机:  
```AppleScript
tell application "PD Runner" to start macvm
```  
列出所有虚拟机:  
```AppleScript
tell application "PD Runner" to list all
```  
启动/停止所有虚拟机:  
```AppleScript
tell application "PD Runner" to start all
tell application "PD Runner" to stop all
```  
隐藏/显示菜单栏图标(下次启动时生效):  
```AppleScript
tell application "PD Runner" to hide icon
tell application "PD Runner" to show icon
```  